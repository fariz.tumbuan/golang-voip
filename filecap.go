package main

import (
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"github.com/google/gopacket/layers"
	"log"
	"time"
)

var (
	pcapFile	string	= "test_180s.pcap"
	handle		*pcap.Handle
	err		error
)

func main() {
	// Open file instead of device
	handle, err = pcap.OpenOffline(pcapFile)
	if err != nil { log.Fatal(err) }
	defer handle.Close()

	// Set filter
	var filter string = "udp"
	err = handle.SetBPFFilter(filter)
	if err != nil { log.Fatal(err) }
	fmt.Println("Only capturing UDP port 80 packets.")

	// Loop through packets in file
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
	for packet := range packetSource.Packets() {
		if udpLayer := packet.Layer(layers.LayerTypeUDP); udpLayer != nil {
			udp, _ := udpLayer.(*layers.UDP)
			fmt.Println("Content: ", udp.Contents)
			fmt.Println("Length: ", udp.Length)
			fmt.Println("SrcPort: ", udp.SrcPort)
			fmt.Println("DstPort: ", udp.DstPort)
			fmt.Println(udp.Payload)
			time.Sleep(5 * time.Second)
			rtpLayer := udp.Payload
			dissectRtp(rtpLayer)
		//	udp, _ := udpLayer.(*layers.UDP)
		//	v_p_x_cc := udp.Payload[0]
		//	v := v_p_x_cc >> 6
		//	cc := v_p_x_cc & 0xf
		//	fmt.Println("v: ", v, "cc: ", cc)
		//	fmt.Println(udp.Payload)
		//	if udp.Length == 180 && v == 2 && cc == 0 {
		//		m_pt := udp.Payload[1]
		//		pt := m_pt &^ (1 << 7)
		//		fmt.Println("pt: ", pt)

		//		seq := (udp.Payload[2] << 8) ^ udp.Payload[3]
		//		fmt.Println("seq: ", seq)

		//		//ssrc := (udp.Payload[8] << 24) ^ (udp.Payload[9] << 16) ^ (udp.Payload[10] << 8) ^ (udp.Payload[11])
		//		//fmt.Println("ssrc: ", bytes(ssrc))
		//		ssrc := formatSsrc(udp.Payload[8:12])
		//		fmt.Println("ssrc ->", ssrc)
		//		data := udp.Payload[12 + 4*cc:]
		//		fmt.Println("data: ", len(data))
		//		fmt.Println("\n")
		//		time.Sleep(500 * time.Millisecond)
		//	}
		}
	}
}

func dissectRtp(rtpLayer []byte) {
	firstOctet := rtpLayer[0]
	version := firstOctet >> 6
	padding := firstOctet & 0x20
	extension := firstOctet & 0x10
	csrc := firstOctet & 0xF

	fmt.Println("version: ", version)
	fmt.Println("padding: ", padding)
	fmt.Println("extension: ", extension)
	fmt.Println("csrc: ", csrc)
}

func formatSsrc(ssrc []byte) uint64 {
	a := (uint64(ssrc[0]) << 24)
	b := (uint64(ssrc[1]) << 16)
	c := (uint64(ssrc[2]) << 8)
	d := uint64(ssrc[3])

	return (a ^ b ^ c ^ d)
}
