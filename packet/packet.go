package voip

import (
	"bytes"
	"encoding/binary"
	"log"
)

// TODO: Implement CsrcIdentifier
type CsrcIdentifier struct {
	dummy int
}

type RtpHeader struct {
	Version		uint8
	Padding		uint8
	Extension	uint8
	CSRCCount	uint8
	Marker		uint8
	PayloadType	uint8
	Sequence	uint32 // Not int to make parsing easier
	TimeStamp	uint32
	SSRC		uint32
	CSRCs		[]CsrcIdentifier
}

type RtpPacket struct {
	Header RtpHeader
	Payload []byte
}

func parseUint32(byteSlice []byte) uint32 {
	result := uint32(0)
	var byteNewBuf []byte
	for _, b := range byteSlice {
		byteNewBuf = append(byteNewBuf, b)
	}
	for len(byteNewBuf) < 4 {
		byteNewBuf = append([]byte{0}, byteNewBuf...)
	}
	buf := bytes.NewBuffer(byteNewBuf)
	err := binary.Read(buf, binary.BigEndian, &result)
	if err != nil {
		log.Fatalf("Decode failed %s", err)
	}
	return result
}

func (rtpPacket *RtpPacket) BuildNew(rawPacket []byte) {
	rtpPacket.Header.Version = rawPacket[0] >> 6
	rtpPacket.Header.Padding = rawPacket[0] & 0x20
	rtpPacket.Header.Extension = rawPacket[0] & 0x10
	rtpPacket.Header.CSRCCount = rawPacket[0] & 0xF
	rtpPacket.Header.Marker = rawPacket[1] & 0x80
	rtpPacket.Header.PayloadType = rawPacket[1] & 0x7F

	seq := []byte {rawPacket[2],rawPacket[3]}
	rtpPacket.Header.Sequence = parseUint32(seq)

	ts := []byte {rawPacket[4], rawPacket[5], rawPacket[6], rawPacket[7]}
	rtpPacket.Header.TimeStamp = parseUint32(ts)

	ssrc := []byte {rawPacket[8], rawPacket[9], rawPacket[10], rawPacket[11]}
	rtpPacket.Header.SSRC = parseUint32(ssrc)

	payloadOffset := 12
	//TODO: Dissect CSRCs. Now is always assumed to 0
	// if cc := rtpPacket.Header.CSRCCount; cc > 0 {
	//	getCSRCs(rtpPacket.Header.CSRCs, rawPacket[12:16+4*cc])
	//	payloadOffset += 16+4*cc
	//}
	rtpPacket.Payload = make([]byte, len(rawPacket[payloadOffset:]))
	copy(rtpPacket.Payload, rawPacket[payloadOffset:])
}
